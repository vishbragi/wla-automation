package page;

import common.AttachHooks;
import common.BasePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import objectrepository.HearingPageLocators;
import objectrepository.HomePageLocators;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuperHearingPage extends BasePage {

    public SuperHearingPage(WebDriver driver) {
        super(driver);
    }

    private static Logger LOGGER = LoggerFactory.getLogger(SuperHearingPage.class);

    AppiumDriver<MobileElement> mobileDriver;
    AndroidDriver<AndroidElement> ad = AttachHooks.getDriver();

    public boolean VerifySuperHearingPage() {
        try {
            LOGGER.info("Verifying Super Hearing Page");
            if (isElementPresent(HearingPageLocators.superHearingTitle, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.closeButton, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.offButton, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.transparency, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.volumeDown, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.volumeUp, WaitElementFor()) &&
                    isElementPresent(HearingPageLocators.volumeSlider, WaitElementFor())) {
                LOGGER.info("All contents are verified successfully on super hearing Page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("All contents were not verified successfully on super hearing Page ::" + e.getMessage());
        }
        LOGGER.error("All contents were not verified successfully on super hearing Page");
        return false;
    }

    public boolean ClickOption(String cardOption) {
        try {
            if (cardOption.equalsIgnoreCase("transparency")) {
                findElement(HearingPageLocators.transparency, WaitElementFor()).click();
                LOGGER.info(cardOption + " card is clicked successfully on super hearing Page");
                return true;
            } else if (cardOption.equalsIgnoreCase("off")) {
                findElement(HearingPageLocators.offButton, WaitElementFor()).click();
                LOGGER.info(cardOption + " card is clicked successfully on super hearing Page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(cardOption + " option was not clicked successfully on super hearing Page ::" + e.getMessage());
        }
        LOGGER.error(cardOption + " option was not clicked successfully on super hearing Page");
        return false;
    }
}
