package page;

import common.AttachHooks;
import common.BasePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import objectrepository.HomePageLocators;
import objectrepository.LandingPageLocators;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    private static Logger LOGGER = LoggerFactory.getLogger(HomePage.class);

    AppiumDriver<MobileElement> mobileDriver;
    AndroidDriver<AndroidElement> ad = AttachHooks.getDriver();


    public boolean VerifyHomePage() {
        try {
            LOGGER.info("Verifying Home Page");
            if (findElement(HomePageLocators.brandLogo, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.addButton, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.settings, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.deviceInfo, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.superHearing, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.actionButton, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.equalizer, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.headMove, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.voiceControl, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.voiceMode, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.brandButton, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.homeButton, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.experienceButton, WaitElementFor()).isDisplayed() &&
                    findElement(HomePageLocators.storeButton, WaitElementFor()).isDisplayed()) {
                LOGGER.info("All contents are verified successfully on Home Page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("All contents were not verified successfully on Home Page ::" + e.getMessage());
        }
        LOGGER.error("All contents were not verified successfully on Home Page");
        return false;
    }

    public boolean ClickCard(String cardName) {
        try {
            if (cardName.equalsIgnoreCase("Super Hearing")) {
                findElement(HomePageLocators.superHearing, WaitElementFor()).click();
                LOGGER.info(cardName + " card is clicked successfully on home Page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(cardName + " card is not clicked successfully on home Page :: " + e.getMessage());
        }
        LOGGER.error(cardName + " card is not clicked successfully on home Page");
        return false;
    }
}
