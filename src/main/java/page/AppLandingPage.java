package page;

import common.AttachHooks;
import common.BasePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.offset.ElementOption;
import objectrepository.LandingPageLocators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppLandingPage extends BasePage {

    public AppLandingPage(WebDriver driver) {
        super(driver);
    }

    private static Logger LOGGER = LoggerFactory.getLogger(AppLandingPage.class);

    AppiumDriver<MobileElement> mobileDriver;
    AndroidDriver<AndroidElement> ad = AttachHooks.getDriver();


    public boolean VerifyLandingPage() {
        try {
            LOGGER.info("Verifying Landing Page");
            if (findElement(LandingPageLocators.titleLandingPage, WaitElementFor()).isDisplayed() &&
                    findElement(LandingPageLocators.termsLandingPage, WaitElementFor()).isDisplayed() &&
                    findElement(LandingPageLocators.slideLandingPage, WaitElementFor()).isDisplayed()) {
                LOGGER.info("Title, Terms and Slides are verified on Landing Page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Title, Terms and Slides are not verified on Landing Page ::" + e.getMessage());
        }
        LOGGER.error("Title, Terms and Slides are not verified on Landing Page");
        return false;
    }

    public boolean SlideToAgree() {
        try {
            if (isElementPresent(LandingPageLocators.slideLandingPage, WaitElementFor())) {
                AndroidTouchAction ta = new AndroidTouchAction(AttachHooks.getDriver());
                AndroidElement slider = ad.findElement(By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_slider']"));
                AndroidElement arrow = ad.findElement(By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/arrow']"));
//                AndroidElement thumb = ad.findElement(By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/thumb']"));

                ta.press(ElementOption.element(slider)).waitAction().moveTo(ElementOption.element(arrow)).release().perform();
                LOGGER.info("Slide is done on landing page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("There was some issue in sliding to agree " + e.getMessage());
        }
        LOGGER.error("There was some issue in sliding to agree");
        return false;
    }

    public boolean VerifyBluetoothSettingsPage() {
        try {
            if (isElementPresent(LandingPageLocators.bluetoothTitle, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothLogo, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothMessage, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothOkButton, WaitElementFor()) &&
                    findElement(LandingPageLocators.bluetoothTitle, WaitElementFor()).isEnabled()) {
                LOGGER.info("Bluetooth settings page is verified for all contents");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Bluetooth settings page is not verified successfully for all contents ::" + e.getMessage());
        }
        LOGGER.error("Bluetooth settings page is not verified successfully verified for all contents");
        return false;
    }

    public boolean VerifyPopUpScreen() {
        try {
            if (isElementPresent(LandingPageLocators.accessLogo, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.accessMessage, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.allowButton, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.denyButton, WaitElementFor())) {
                LOGGER.info("Pop up screen is verified for all contents");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Pop up screen is not verified successfully for all contents ::" + e.getMessage());
        }
        LOGGER.error("Pop up screen is not verified successfully verified for all contents");
        return false;
    }

    public boolean ClickButtons(String btnType) {
        try {
            if (btnType.equalsIgnoreCase("Ok")) {
                findElement(LandingPageLocators.bluetoothOkButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on bluetooth settings page");
                return true;
            } else if (btnType.equalsIgnoreCase("Continue")) {
                findElement(LandingPageLocators.bluetoothOkButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on file access page");
                return true;
            } else if (btnType.equalsIgnoreCase("Allow")) {
                findElement(LandingPageLocators.allowButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on pop up screen");
                return true;
            } else if (btnType.equalsIgnoreCase("Allow Only")) {
                findElement(LandingPageLocators.allowOnlyButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on pop up screen");
                return true;
            } else if (btnType.equalsIgnoreCase("Deny")) {
                findElement(LandingPageLocators.denyButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on pop up screen");
                return true;
            } else if (btnType.equalsIgnoreCase("Connected")) {
                findElement(LandingPageLocators.connectedButton, WaitElementFor()).click();
                LOGGER.info(btnType + " button is clicked successfully on Connected page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("There was some issue in clicking ok button on bluetooth settings page ::" + e.getMessage());
        }
        LOGGER.error("There was some issue in clicking ok button on bluetooth settings page");
        return false;
    }

    public boolean ConnectHeadphone() {
        try {
            waitForElementToBePresent(LandingPageLocators.bluetoothTitle, WaitElementFor());
            if (isElementPresent(LandingPageLocators.bluetoothTitle, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.deviceName, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.connectButton, WaitElementFor())) {
                findElement(LandingPageLocators.connectButton, WaitElementFor()).click();
                LOGGER.info("Connect button is clicked successfully after verifying the page");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Connect button was not clicked successfully after verifying the page ::" + e.getMessage());
        }
        LOGGER.error("Connect button was not clicked successfully after verifying the page");
        return false;
    }

    public boolean VerifyFileAccessPage() {
        try {
            if (isElementPresent(LandingPageLocators.bluetoothTitle, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothLogo, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothMessage, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothOkButton, WaitElementFor()) &&
                    findElement(LandingPageLocators.bluetoothTitle, WaitElementFor()).isEnabled()) {
                LOGGER.info("File Access page is verified for all contents");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("File Access page is not verified successfully for all contents ::" + e.getMessage());
        }
        LOGGER.error("File Access page is not verified successfully verified for all contents");
        return false;
    }

    public boolean VerifyConnectedPage() {
        try {
            if (isElementPresent(LandingPageLocators.bluetoothTitle, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothLogo, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.bluetoothMessage, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.subMessage, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.connectedButton, WaitElementFor()) &&
                    isElementPresent(LandingPageLocators.settingsButton, WaitElementFor())) {
                LOGGER.info("Connected page is verified for all contents");
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Connected page is not verified successfully for all contents ::" + e.getMessage());
        }
        LOGGER.error("Connected page is not verified successfully verified for all contents");
        return false;
    }

}

