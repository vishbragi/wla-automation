package Bean;

public class APIGetBean {

    private static String landingPageText;
    private static String signInText;
    private static String createAccountText;
    private static String continueGuestText;

    public static void setLandingPageText(String data) {

        landingPageText = data;
    }

    public static String getLandingPageText() {

        return landingPageText;
    }

    public static void setSignInText(String data) {

        signInText = data;
    }

    public static String getSignInText() {

        return signInText;
    }

    public static void setCreateAccountText(String data) {

        createAccountText = data;
    }

    public static String getCreateAccountText() {

        return createAccountText;
    }

    public static void setContinueGuestText(String data) {

        continueGuestText = data;
    }

    public static String getContinueGuestText() {

        return continueGuestText;
    }

}
