package Bean;

import java.util.List;

public class DataBean {

    private static List<String> articleID;
    private static int jobCount;
    private static List<String> jobListFav;
    private static int count;
    private static int jobFavCountOnSearchResult;
    private static String scenarioName;
    private static String candidateEmailId;
    private static String verificationCode;
    private static String envId;
    private static int filterValue;
    private static String firstName;
    private static String lastName;
    private static String chatDescription;
    private static String leftFooter;
    private static String rightFooter;
    private static String headerIcon;
    private static String headerOptions;
    private static String cardsHomePage;
    private static int yearListValue;
    private static int toBeAddedExpCount;
    private static int companyNameLength;
    private static int jobTitleLength;
    private static int roleDescriptionLength;
    private static int enteredCompanyNameLength;
    private static int enteredJobTitleLength;
    private static int enteredRoleDescriptionLength;
    private static String startDateMonth;
    private static String endDateMonth;
    private static String flag;
    private static String workExpData;
    private static String jobTitle;
    private static String applicationData;
    private static String filterView;
    private static String languageHeader;
    private static String signInButtonText;


    public static void setJobResultCount(int aID) {

        jobCount = aID;
    }

    public static int getJobResultCount() {

        return jobCount;
    }

    public static void setArticleID(List<String> aID) {

        articleID = aID;
    }

    public static List<String> getArticleID() {

        return articleID;
    }

    public static void setJobListFav(List<String> jobID) {

        jobListFav = jobID;
    }

    public static List<String> getJobListFav() {

        return jobListFav;
    }

    public static void setCount(int workExpCount) {

        count = workExpCount;
    }

    public static int getCount() {

        return count;
    }

    public static void setScenarioName(String scenario) {

        scenarioName = scenario;
    }

    public static String getScenarioName() {

        return scenarioName;
    }

    public static void setEmailId(String emailId) {

        candidateEmailId = emailId;
    }

    public static String getEmailId() {

        return candidateEmailId;
    }

    public static void setJobFavCountOnSearchResult(int jobFavCount) {

        jobFavCountOnSearchResult = jobFavCount;
    }

    public static int getJobFavCountOnSearchResult() {

        return jobFavCountOnSearchResult;
    }

    public static void setVerificationCode(String code) {

        verificationCode = code;
    }

    public static String getVerificationCode() {

        return verificationCode;
    }

    public static void setEnvId(String arg) {

        envId = arg;
    }

    public static String getEnvId() {

        return envId;
    }

    public static void setFilterValue(int filterCount) {

        filterValue = filterCount;
    }

    public static int getFilterValue() {

        return filterValue;
    }

    public static void setFirstName(String fName) {

        firstName = fName;
    }

    public static String getFirstName() {

        return firstName;
    }

    public static void setLastName(String lName) {

        lastName = lName;
    }

    public static String getLastName() {

        return lastName;
    }

    public static void setChatDescription(String chatDesc) {

        chatDescription = chatDesc;
    }

    public static String getChatDescription() {

        return chatDescription;
    }

    public static void setLeftFooterData(String data) {

        leftFooter = data;
    }

    public static String getLeftFooterData() {

        return leftFooter;
    }

    public static void setRightFooterData(String data) {

        rightFooter = data;
    }

    public static String getRightFooterData() {

        return rightFooter;
    }

    public static void setHeaderIconData(String headerData) {

        headerIcon = headerData;
    }

    public static String getHeaderIconData() {

        return headerIcon;
    }

    public static void setHeaderOptionsData(String headerOptionsData) {

        headerOptions = headerOptionsData;
    }

    public static String getHeaderOptionsData() {

        return headerOptions;
    }

    public static void setCardsHomePage(String cardsData) {

        cardsHomePage = cardsData;
    }

    public static String getCardsHomePage() {

        return cardsHomePage;
    }

    public static void setYearCount(int yearCount) {

        yearListValue = yearCount;
    }

    public static int getYearCount() {

        return yearListValue;
    }

    public static void setAddedWorkExpCount(int count) {

        toBeAddedExpCount = count;
    }

    public static int getAddedWorkExpCount() {

        return toBeAddedExpCount;
    }

    public static void setCompanyNameLength(int count) {

        companyNameLength = count;
    }

    public static int getCompanyNameLength() {

        return companyNameLength;
    }

    public static void setJobTitleLength(int count) {

        jobTitleLength = count;
    }

    public static int getJobTitleLength() {

        return jobTitleLength;
    }

    public static void setRoleDescriptionLength(int count) {

        roleDescriptionLength = count;
    }

    public static int getRoleDescriptionLength() {

        return roleDescriptionLength;
    }

    public static void setEnteredCompanyNameLength(int count) {

        enteredCompanyNameLength = count;
    }

    public static int getEnteredCompanyNameLength() {

        return enteredCompanyNameLength;
    }

    public static void setEnteredJobTitleLength(int count) {

        enteredJobTitleLength = count;
    }

    public static int getEnteredJobTitleLength() {

        return enteredJobTitleLength;
    }

    public static void setEnteredRoleDescriptionLength(int count) {

        enteredRoleDescriptionLength = count;
    }

    public static int getEnteredRoleDescriptionLength() {

        return enteredRoleDescriptionLength;
    }

    public static void setStartDateMonth(String startMonth) {

        startDateMonth = startMonth;
    }

    public static String getStartDateMonth() {

        return startDateMonth;
    }

    public static void setEndDateMonth(String endMonth) {

        endDateMonth = endMonth;
    }

    public static String getEndDateMonth() {

        return endDateMonth;
    }

    public static void setFlag(String flagValue) {

        flag = flagValue;
    }

    public static String getFlag() {

        return flag;
    }

    public static void setWorkExpData(String data) {

        workExpData = data;
    }

    public static String getWorkExpData() {

        return workExpData;
    }

    public static void setJobTitle(String data) {

        jobTitle = data;
    }

    public static String getJobTitle() {

        return jobTitle;
    }

    public static void setApplicationData(String data) {

        applicationData = data;
    }

    public static String getApplicationData() {

        return applicationData;
    }

    public static void setFilterView(String data) {

        filterView = data;
    }

    public static String getFilterView() {

        return filterView;
    }

    public static void setLanguageHeader(String data) {

        languageHeader = data;
    }

    public static String getLanguageHeader() {

        return languageHeader;
    }

    public static void setLanguageSignInButton(String data) {

        signInButtonText = data;
    }

    public static String getLanguageSignInButton() {

        return signInButtonText;
    }

}
