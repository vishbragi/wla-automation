package objectrepository;

import org.openqa.selenium.By;

public class HearingPageLocators {

    public static final By superHearingTitle = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_superhearing_title']");

    public static final By closeButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_close']");

    public static final By offButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/item_off']");

    public static final By transparency = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/item_transparency']");

    public static final By volumeDown = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/iv_volume_down']");

    public static final By volumeUp = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/iv_volume_up']");

    public static final By volumeSlider = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/seek_bar_transparency']");

    public static final By mediaInfo = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_media_info']");

    public static final By onCalls = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_calls_title']");

    public static final By checkMedia = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/check_media']");

    public static final By checkCalls = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/check_calls']");

}
