package objectrepository;

import Bean.DataBean;
import org.openqa.selenium.By;

public class LandingPageLocators {

    public static final String locator = DataBean.getEnvId();

    public static final By titleLandingPage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_title']");

    public static final By termsLandingPage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_terms']");

    public static final By slideLandingPage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_slider']");

    public static final By slideThumbLandingPage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/thumb']");

    public static final By arrowLandingPage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/arrow']");

    public static final By bluetoothTitle = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_title']");

    public static final By bluetoothLogo = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/iv_icon']");

    public static final By bluetoothMessage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_info']");

    public static final By bluetoothOkButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_ok']");

    public static final By accessLogo = By.xpath("//*[@resource-id='com.android.permissioncontroller:id/permission_icon']");

    public static final By accessMessage = By.xpath("//*[@resource-id='com.android.permissioncontroller:id/permission_message']");

    public static final By allowButton = By.xpath("//*[@resource-id='com.android.permissioncontroller:id/permission_allow_button']");

    public static final By allowOnlyButton = By.xpath("//*[@resource-id='com.android.permissioncontroller:id/permission_allow_foreground_only_button']");
    public static final By denyButton = By.xpath("//*[@resource-id='com.android.permissioncontroller:id/permission_deny_button']");

    public static final By subMessage = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/tv_sub_info']");

    public static final By connectedButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_connected']");

    public static final By settingsButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_settings']");
    public static final By deviceName = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/earbudsNameTextView']");

    public static final By connectButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/connectButtonTextView']");

}
