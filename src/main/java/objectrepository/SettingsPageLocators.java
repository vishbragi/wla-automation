package objectrepository;

import org.openqa.selenium.By;

public class SettingsPageLocators {

    public static final By enableBluetooth = By.xpath("//*[@resource-id='android:id/checkbox']");

    public static final By goBackButton = By.xpath("//*[@resource-id='miui:id/up']");

}
