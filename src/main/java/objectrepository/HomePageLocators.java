package objectrepository;

import org.openqa.selenium.By;

public class HomePageLocators {

    public static final By brandLogo = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/imageView3']");

    public static final By addButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_add']");

    public static final By settings = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/btn_settings']");

    public static final By deviceInfo = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/cv_earbuds_info']");

    public static final By voiceMode = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/cv_vm']");

    public static final By brandButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/brand']");

    public static final By homeButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/home']");

    public static final By experienceButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/experiences']");

    public static final By storeButton = By.xpath("//*[@resource-id='com.bragi.wla.vanilla.dev:id/bragi']");

    public static final By superHearing = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout");

    public static final By actionButton = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout");

    public static final By equalizer = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[4]/android.widget.FrameLayout");

    public static final By headMove = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[5]/android.widget.FrameLayout");

    public static final By voiceControl = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[6]/android.widget.FrameLayout");


}
