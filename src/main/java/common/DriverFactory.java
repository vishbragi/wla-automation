package common;

import java.io.IOException;

public class DriverFactory {
    static Process process;

    /**
     * This method Is responsible for starting appium server on windows.
     */
    public static void appiumStartWindows() {
        //String[] command = { "cmd.exe", "/C", "Start", "appium" };

        String[] command = { "bash", "-c", "adb devices" };

        ProcessBuilder pb;
        if (process == null) {
            pb = new ProcessBuilder(command);
            try {
                process = pb.start();
                Thread.sleep(20000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        if (process != null) {
            System.out.println("SERVER STARTED");
        }
    }

    /**
     * This method Is responsible for starting appium server on mac.
     */
    public static void appiumStartMac() throws IOException{
//        String[] command = { "osascript", "Terminal.scpt" };


        Runtime.getRuntime().exec("osascript "+"Terminal1.scpt");

        System.out.println("JAVA_HOME set path");
        try {
            Runtime.getRuntime().exec("osascript "+"Terminal.scpt");
            System.out.println("Appium server started again");
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        final ProcessBuilder processBuilder;
//        if (process == null) {
//            try {
//                processBuilder = new ProcessBuilder(command);
//                process = processBuilder.start();
//                process.waitFor();
//                Thread.sleep(20000);
//
//            } catch (IOException | InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        if (process != null) {
//            System.out.println("SERVER STARTED");
//        }
    }

}

