package common;

import Bean.DataBean;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AttachHooks {

    private Scenario scenario;
    private static WebDriver driver;

    private static ThreadLocal<AndroidDriver> dr = new ThreadLocal<>();

    public static AndroidDriver getDriver() {
        return dr.get();
    }

    public static void setDriver(AndroidDriver driverRef) {
        dr.set(driverRef);
    }

    public static void unload() {
        dr.remove();
    }

    DesiredCapabilities capabilities;
    private static Logger LOGGER = LoggerFactory.getLogger(AttachHooks.class);
    public static final String webId = "id";
    public static final String appId = "resource-id";

    //for extent external report only

    ExtentReports extent = new ExtentReports();

    ExtentTest test;

    ExtentHtmlReporter htmlReporter;

    //    static String videoFolder = "/Users/vishalverma/Desktop/Automation/GlobalAppAutomation/target";
    static String videoFolder = "/Users/runner/work/1/s/target";

    public void startAppiumService(String portNumber) {
        AppiumDriverLocalService service;
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(Integer.parseInt(portNumber));
        service = AppiumDriverLocalService.buildService(builder);
        service.start();
        LOGGER.info("Service has been started with port number: " + portNumber);
    }

    public String findFile(String name) {
        File folder = new File(videoFolder);
        boolean isFound = false;
        File finalFile;
        while (!isFound) {
            File[] files = folder.listFiles();
            for (File file : files) {
                if (file.getName().contains(name)) {
                    LOGGER.info("File -> " + file.getName());
                    isFound = true;
                    DataBean.setScenarioName(file.getName());
                    finalFile = file;
                    break;
                }
            }
        }
        return DataBean.getScenarioName();
    }

    @Before
//    @Parameters(value = {"browser"})
    public void setUp(Scenario scenario) throws IOException, Exception {
        LOGGER.info("Inside set up method of before hook");
        LOGGER.info(System.getProperty("user.dir"));
        LOGGER.info("setup:::::::");
        ConfigManager.loadConfig();
        this.scenario = scenario;

        System.out.println(scenario.getName());
        String path;

        /**
         * This method Is responsible for executing test cases on Native Mobile
         * apps.
         */
        if (ConfigManager.getProperty("ExecutionPlatform").equalsIgnoreCase("Mobile")) {
//            if (ConfigManager.getProperty("MachineName").equalsIgnoreCase("Windows")) {
//                DriverFactory.appiumStartWindows();
//            } else if (ConfigManager.getProperty("MachineName").equalsIgnoreCase("MAC")) {
//                DriverFactory.appiumStartMac();
//            }
            path = System.getProperty("user.dir") + ConfigManager.getProperty("ApkPath");
            DataBean.setEnvId(appId);
//            startAppiumService(Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("portNumber"));
            if (ConfigManager.getProperty("PlatformName").equalsIgnoreCase("Android")) {
                boolean DriverNoResetFlag = true;
                boolean locationServiceEnabled = true;
                capabilities = new DesiredCapabilities();
                //capabilities.setCapability("emulator", true);
                //Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("deviceName");
                //Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("platformVersion");
//                Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("portNumber");
               // capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
                 //       Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("deviceName"));


                //capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
                //        ConfigManager.getProperty("PlatformName"));
                //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
                //        Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("platformVersion"));
                capabilities.setCapability("automationName", "UiAutomator2");
                capabilities.setCapability(MobileCapabilityType.APP, path);
//                capabilities.setCapability("adbExecTimeout", 90000);
//                capabilities.setCapability("uiautomator2ServerInstallTimeout",90000);
//                capabilities.setCapability("androidInstallTimeout",90000);
                capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60000");
                capabilities.setCapability("waitForQuiescence", false);

                if (DriverNoResetFlag) {
                    capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
                    capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
                    // capabilities.setCapability("noReset", DriverNoResetFlag);
                }
                if (locationServiceEnabled = true) {
                    capabilities.setCapability("autoAcceptAlerts", true);
                }

                try {
//                    driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                    //Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("portNumber")
                    setDriver(new AndroidDriver(new URL("http://127.0.0.1:" + 4723 + "/wd/hub"), capabilities));

//                    setDriver(driver);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                //AttachHooks.getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            }
//            } else if (ConfigManager.getProperty("PlatformName").equalsIgnoreCase("iOS")) {
////                RepositoryParser.loadJson(ConfigManager.getProperty("iosJson"));
//                path = System.getProperty("user.dir") + ConfigManager.getProperty("iOSApkPath");
//                boolean DriverNoResetFlag = true;
//                boolean locationServiceEnabled = true;
//                capabilities = new DesiredCapabilities();
//                capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
//                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
//                        ConfigManager.getProperty("PlatformVersion"));
//                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
//                        ConfigManager.getProperty("PlatformName"));
//                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ConfigManager.getProperty("DeviceName"));
//                capabilities.setCapability(MobileCapabilityType.APP, ConfigManager.getProperty("iOSApkPath"));
//                capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60000");
//                capabilities.setCapability(MobileCapabilityType.UDID, "6A3CDBA9-FE65-4503-B8FD-E7FE418B00D7");
//                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
//                capabilities.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 500000);
//                if (DriverNoResetFlag) {
//                    capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
//                    capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
//                    // capabilities.setCapability("noReset", DriverNoResetFlag);
//                }
//                if (locationServiceEnabled = true) {
//                    capabilities.setCapability("autoAcceptAlerts", true);
//                }
//                try {
//                    //	driver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
////                    driver = new IOSDriver(new URL("http://127.0.0.1:7777/wd/hub"), capabilities);
////                    setDriver(new IOSDriver<>(new URL("http://127.0.0.1:" + Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("portNumber") + "/wd/hub"), capabilities));
//
//                    AttachHooks.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }

    /**
     * This method Is responsible for stopping test case and embedding
     * screenshot on failure
     *
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IOException
     */
    @After
    public void tearDown() throws InstantiationException, IllegalAccessException, IOException, InterruptedException, Exception {
//        if (ConfigManager.getProperty("ExecutionPlatform").equalsIgnoreCase("Mobile")) {
//        MyScreenRecorder.stopRecording();

        if (scenario.isFailed()) {
//            findFile(scenario.getName().replaceAll(" ", "_"));

//            findFile(scenario.getName());

            // Take a screenshot...
            final byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
//            String video = "<a href='" + videoFolder + "/" + DataBean.getScenarioName()
//                    + "'>" + "<span class='label info'> <h5><div><marquee direction='right' behavior='alternate'><b>Click Me to Download Test Video of <br>" + scenario.getName() + "</br></b></marquee></h5></span></a>";

//            scenario.embed(video.getBytes(StandardCharsets.UTF_8), "text/html");

//            Reporter.loadXMLConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
//
//            Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
//
//            Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
//
//            Reporter.setSystemInfo("Machine", "MACOX" + "64 Bit");
//
//
//
//            Reporter.addScenarioLog("<a href='" + videoFolder + "/" + DataBean.getScenarioName()
//                    + "'>" + "<span class='label info'> <h5><div><marquee direction='right' behavior='alternate'><b>Click Me to Download Test Video of <br>" + scenario.getName() + "</br></b></marquee></h5></span></a>");


        } else if (!scenario.isFailed()) {
            // Take a screenshot...
            final byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
        }

        getDriver().quit();
        unload();
//        }

    }

}