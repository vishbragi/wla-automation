package common;

import io.appium.java_client.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BasePage {

    protected static WebDriver driver;
    static WebDriverWait wait;
    private static Logger LOGGER = LoggerFactory.getLogger(BasePage.class);


    public BasePage(WebDriver driver) {
        BasePage.driver = AttachHooks.getDriver();
    }

    public WebElement waitForWebElementPresent(WebElement element, int timeoutSeconds) {
        wait = new WebDriverWait(AttachHooks.getDriver(), timeoutSeconds);
        WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(element));
        if (elem != null) {
            return elem;
        } else {
            return null;
        }
    }

    public WebElement WaitForElementVisibility(By locator, int timeoutSeconds) {
        wait = new WebDriverWait(AttachHooks.getDriver(), timeoutSeconds);
        WebElement elem = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        if (elem != null) {
            return elem;
        } else {
            return null;
        }
    }


    public static void scrollIntoViewElement(WebDriver driver, WebElement element) {

        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
    }

    //Common Method created to check if element is present - Vishal
    public boolean isElementPresent(By locator, int timeoutInSeconds) {
        try {
            wait = new WebDriverWait(AttachHooks.getDriver(), timeoutInSeconds);
            WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
            if (elem != null) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    //Common Method created to find an element
    public WebElement findElement(By locator, int timeoutSeconds) {
        wait = new WebDriverWait(AttachHooks.getDriver(), timeoutSeconds);
        WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        if (elem != null) {
            return elem;
        } else {
            return null;
        }
    }

    //Common Method created to find an element
    public WebElement waitForElementToBePresent(By locator, int timeoutSeconds) {
        wait = new WebDriverWait(AttachHooks.getDriver(), timeoutSeconds);
        WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(locator));
        if (elem != null) {
            return elem;
        } else {
            return null;
        }
    }

    public boolean WaitTime(long time) {
        long startTime = System.currentTimeMillis();
        long waitTime = Long.parseLong(ConfigManager.getProperty("WaitTime")) * 60000;
        long endTime = time + waitTime;
        if (startTime < endTime) {
            return true;
        } else {
            return false;
        }
    }

//    public long startTime() {
//        long startTime = System.currentTimeMillis();
//        return startTime;
//    }

    public String TotalTime(String pageName, double startTime) {
        double endTime = System.currentTimeMillis();
        double total = (endTime - startTime) / 1000;
        LOGGER.info("Total time taken for " + pageName + " is :::: " + total);
        return String.valueOf(total);
    }

    public boolean CSVWriter(String pageName, double startTime) {
        try {
            FileWriter csvWriter = new FileWriter("Sample.csv", true);
            String pageLoadTime = TotalTime(pageName, startTime);
            List<List<String>> rows = Arrays.asList(
                    Arrays.asList(pageName, pageLoadTime)
            );
//            csvWriter.append("PageName");
//            csvWriter.append(",");
//            csvWriter.append("PageLoadTime");
//            csvWriter.append("\n");
            for (List<String> rowData : rows) {
                csvWriter.append(String.join(",", rowData));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Integer LessWaitElementFor() {
        int timeInSecond = Integer.parseInt(ConfigManager.getProperty("LessWaitForElementTime"));
        return timeInSecond;
    }

    public Integer WaitElementFor() {
        int timeInSecond = Integer.parseInt(ConfigManager.getProperty("WaitForElementTime"));
        return timeInSecond;
    }

    public Integer LongWaitElementFor() {
        int timeInSecond = Integer.parseInt(ConfigManager.getProperty("LongWaitForElementTime"));
        return timeInSecond;
    }

    public void getscreenshot(AppiumDriver<MobileElement> d) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        Date date = new Date();
        String fileName = sdf.format(date);
        File des = d.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(des, new File(System.getProperty("user.dir") + "/target/" + fileName + ".png"));
        System.out.println("Screenshot is captured");
    }

    public static String greetingMessage() throws UnsupportedEncodingException {
        int hours = ZonedDateTime.now().getHour();
        if (hours >= 4 && hours < 12) {
            return LocaleMsgReader.getString("MorningMessage");
        } else if (hours >= 12 && hours < 17) {
            return LocaleMsgReader.getString("AfternoonMessage");
        } else {
            return LocaleMsgReader.getString("EveningMessage");
        }
    }

    public static void clearDataJs(WebElement element, String a) {
        ((JavascriptExecutor) AttachHooks.getDriver()).executeScript("arguments[0].value='" + a + "';", element);

    }

    public MobileElement mobileScrollToExactElement(String str) {
        MobileElement element = (MobileElement) AttachHooks.getDriver().findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector().text(\"" + str + "\"))"));
        return element;
    }

    public MobileElement scrollTo(String text) {
        MobileElement element = (MobileElement) AttachHooks.getDriver().findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))"));
        return element;
    }

    public static void scrollToMiddle() {
        Dimension dimension = AttachHooks.getDriver().manage().window().getSize();
        int start_x = (int) (dimension.width * 0.5);
        int start_y = (int) (dimension.height * 0.5);

        int end_x = (int) (dimension.width * 0.2);
        int end_y = (int) (dimension.height * 0.2);

        TouchAction touch = new TouchAction((PerformsTouchActions) AttachHooks.getDriver());
        touch.press(PointOption.point(start_x, start_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(end_x, end_y)).release().perform();

    }

    public static void scrollToUp() {
        Dimension dimension = AttachHooks.getDriver().manage().window().getSize();
        int start_x = (int) (dimension.width * 0.2);
        int start_y = (int) (dimension.height * 0.2);

        int end_x = (int) (dimension.width * 0.5);
        int end_y = (int) (dimension.height * 0.5);

        TouchAction touch = new TouchAction((PerformsTouchActions) AttachHooks.getDriver());
        touch.press(PointOption.point(start_x, start_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(end_x, end_y)).release().perform();

    }

    public static double getRandomIntegerBetweenRange(double min, double max) {
        double x = (int) (Math.random() * ((max - min) + 1)) + min;
        return x;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isElementNotPresent(By locator, int timeoutInSeconds) {
        try {
            wait = new WebDriverWait(AttachHooks.getDriver(), timeoutInSeconds);
            Boolean elem = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
            if (elem != false) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }
}