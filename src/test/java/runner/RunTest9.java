package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports9/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport9.json",
        "rerun:target/cucumber-reports9/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report9/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression9"},

        monochrome = true)
public class RunTest9 extends AbstractTestNGCucumberTests {
};
