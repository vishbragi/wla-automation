package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports5/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport5.json",
        "rerun:target/cucumber-reports5/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report5/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression5"},

        monochrome = true)
public class RunTest5 extends AbstractTestNGCucumberTests {
};
