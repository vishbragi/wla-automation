package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports11/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport11.json",
        "rerun:target/cucumber-reports11/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report11/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression11"},

        monochrome = true)
public class RunTest11 extends AbstractTestNGCucumberTests {
};
