package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports10/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport10.json",
        "rerun:target/cucumber-reports10/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report10/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression10"},

        monochrome = true)
public class RunTest10 extends AbstractTestNGCucumberTests {
};
