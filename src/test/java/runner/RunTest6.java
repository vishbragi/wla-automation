package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports6/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport6.json",
        "rerun:target/cucumber-reports6/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report6/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression6"},

        monochrome = true)
public class RunTest6 extends AbstractTestNGCucumberTests {
};
