package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports7/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport7.json",
        "rerun:target/cucumber-reports7/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report7/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression7"},

        monochrome = true)
public class RunTest7 extends AbstractTestNGCucumberTests {
};
