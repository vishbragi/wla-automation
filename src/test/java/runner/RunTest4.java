package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports4/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport4.json",
        "rerun:target/cucumber-reports4/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report4/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression4"},

        monochrome = true)
public class RunTest4 extends AbstractTestNGCucumberTests {
};
