package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports2/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport2.json",
        "rerun:target/cucumber-reports2/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report2/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regressiont"},

        monochrome = true)
public class RunTest2 extends AbstractTestNGCucumberTests {
};
