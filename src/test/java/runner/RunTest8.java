package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports8/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport8.json",
        "rerun:target/cucumber-reports8/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report8/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression8"},

        monochrome = true)
public class RunTest8 extends AbstractTestNGCucumberTests {
};
