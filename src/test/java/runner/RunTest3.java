package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports3/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport3.json",
        "rerun:target/cucumber-reports3/rerun.txt",
        "com.cucumber.listener.ExtentCucumberFormatter:target/extent.html",
        "json:target/json-report3/cucumber.json"},
        glue = "",
        features = "src/test/resources/",
        dryRun = false,
        tags = {"@Regression3"},

        monochrome = true)
public class RunTest3 extends AbstractTestNGCucumberTests {
};
