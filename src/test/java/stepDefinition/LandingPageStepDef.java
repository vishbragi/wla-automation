package stepDefinition;

import common.AttachHooks;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import page.AppLandingPage;

public class LandingPageStepDef {
    AppLandingPage appLandingPage = new AppLandingPage(AttachHooks.getDriver());

    private static Logger LOGGER = LoggerFactory.getLogger(LandingPageStepDef.class);

    @Given("^Open bragi WLA app and verify Landing page$")
    public void Open_bragi_WLA_app_and_verify_Landing_page() throws Throwable {
        if (appLandingPage.VerifyLandingPage()) {
            LOGGER.info("Landing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @And("^Slide to agree$")
    public void Slide_to_agree() throws Throwable {
        if (appLandingPage.SlideToAgree()) {
            LOGGER.info("Slide is done successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Verify bluetooth Settings page$")
    public void Verify_Bluetooth_Settings_Page() throws Throwable {
        if (appLandingPage.VerifyBluetoothSettingsPage()) {
            LOGGER.info("Bluetooth settings page is verified successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Verify file access page$")
    public void Verify_File_Access_Page() throws Throwable {
        if (appLandingPage.VerifyFileAccessPage()) {
            LOGGER.info("File Access page is verified successfully");
        } else {
            Assert.fail();
        }
    }

    @And("^Verify Access screen pop up$")
    public void Verify_Request_Screen() throws Throwable {
        if (appLandingPage.VerifyPopUpScreen()) {
            LOGGER.info("Pop up screen is verified successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Click on (.*) button$")
    public void Click_Access_Buttons(String btnType) throws Throwable {
        if (appLandingPage.ClickButtons(btnType)) {
            LOGGER.info(btnType + " button is clicked successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Select your headphone and click connect$")
    public void Select_and_click_connect() throws Throwable {
        if (appLandingPage.ConnectHeadphone()) {
            LOGGER.info("Headphone connect button is clicked successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Verify Already connected page$")
    public void Verify_Already_Connected_Page() throws Throwable {
        if (appLandingPage.VerifyConnectedPage()) {
            LOGGER.info("Connected page is verified successfully");
        } else {
            Assert.fail();
        }
    }
}
