package stepDefinition;

import common.AttachHooks;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import page.AppLandingPage;
import page.HomePage;

public class HomePageStepDef {

    HomePage homePage = new HomePage(AttachHooks.getDriver());

    private static Logger LOGGER = LoggerFactory.getLogger(HomePageStepDef.class);

    @Then("^Verify Home page$")
    public void Verify_Home_page() throws Throwable {
        if (homePage.VerifyHomePage()) {
            LOGGER.info("Home Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Click on (.*) card$")
    public void Click_on_given_card(String cardName) throws Throwable {
        if (homePage.ClickCard(cardName)) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

}
