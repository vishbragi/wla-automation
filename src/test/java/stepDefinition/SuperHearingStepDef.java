package stepDefinition;

import common.AttachHooks;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import page.AppLandingPage;
import page.SuperHearingPage;

public class SuperHearingStepDef {
    SuperHearingPage superHearingPage = new SuperHearingPage(AttachHooks.getDriver());

    private static Logger LOGGER = LoggerFactory.getLogger(SuperHearingStepDef.class);

    @Then("^Verify Super Hearing Page$")
    public void Verify_Super_Hearing_Page() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Then("^Click on (.*) option$")
    public void Click_on_option(String optionName) throws Throwable {
        if (superHearingPage.ClickOption(optionName)) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Slide super hearing page$")
    public void Slide_super_hearing_page() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Verify New available options$")
    public void Verify_New_available_options() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Select check media check box$")
    public void Select_check_media_check_box() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Remove check from check media check box$")
    public void Remove_check_box() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

    @Given("^Click close button$")
    public void Click_close_button() throws Throwable {
        if (superHearingPage.VerifySuperHearingPage()) {
            LOGGER.info("Super Hearing Page is verified Successfully");
        } else {
            Assert.fail();
        }
    }

}
