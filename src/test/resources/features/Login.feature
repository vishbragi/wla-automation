Feature: Login

  @vvvv
  Scenario: Validate connecting bluetooth device
    Given Open bragi WLA app and verify Landing page
    And Slide to agree
    Then Verify bluetooth Settings page
    And Click on Ok button
    Then Verify file access page
    And Click on Continue button
    Then Verify Access screen pop up
    And Click on Deny button
    Then Verify file access page
    Then Click on Continue button
    And Click on Allow button
    And Click on Allow Only button
    Then Select your headphone and click connect
    Then Verify Home page

    And Click on Super Hearing card
    Then Verify Super Hearing Page

    Then Click on transparency option
    Then Click on Off option
    Then Slide super hearing page
    And Verify New available options
    Then Select check media check box
    And Remove check from check media check box
    Then Click close button


